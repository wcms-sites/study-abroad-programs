core = 7.x
api = 2

; nodeblock
projects[nodeblock][type] = "module"
projects[nodeblock][download][type] = "git"
projects[nodeblock][download][url] = "https://git.uwaterloo.ca/drupal-org/nodeblock.git"
projects[nodeblock][download][tag] = "7.x-1.2"
projects[nodeblock][subdir] = ""

; uw_ct_exchange_program
projects[uw_ct_exchange_program][type] = "module"
projects[uw_ct_exchange_program][download][type] = "git"
projects[uw_ct_exchange_program][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_exchange_program.git"
projects[uw_ct_exchange_program][download][tag] = "7.x-1.0-beta15"
projects[uw_ct_exchange_program][subdir] = ""
